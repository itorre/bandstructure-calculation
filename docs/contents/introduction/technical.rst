Technical foundations of the package
=================================================
Pybandstructure is suited to calculate physical quantities of periodic models. Any periodic model is
characterized by a basis of primitive translation vectors [1]_
:math:`\bm{t}_{1}` and :math:`\bm{t}_{2}`, whose integer linear
combinations

.. math:: \bm{t}_{n} = n_{1}\bm{t}_{1} + n_{2}\bm{t}_{2} \quad \text{with } n = (n_{1},n_{2}) \in \mathbb{Z}\times\mathbb{Z}
       :label: eq:NUM_primitive_translation

span a Bravais lattice. Accordingly, it is important to consider also
the reciprocal lattice in the dual (or reciprocal) space. Indeed, the
propagation wavevector :math:`\bm{k}` of a general plane wave
:math:`\exp (i \bm{k}\cdot\bm{r})` has reciprocal length dimension, and
can be conveniently represented in the reciprocal space. The reciprocal
lattice is spanned by integer linear combinations :math:`\bm{G}_{n}` of
the vectors :math:`\bm{G}_{1}` and :math:`\bm{G}_{2}`, which are
obtained from the primitive translation vectors through the relation
:math:`\bm{t}_{i}\cdot\bm{G}_{j} = 2\pi\delta_{ij}`.

The Brillouin Zone (BZ) is the region of the reciprocal space with the
property that any point included in the BZ is closer to a chosen lattice
point (say :math:`\bm{G}_{n} = \bm{0}`) than to any other. The energy
eigenvalues of a translational invariant model are labeled by the
crystal wave vector :math:`\bm{k}`, which is inside the Brillouin zone.

To numerically compute the electronic states of a periodic model, one
needs the matrix elements of its Hamiltonian for an orthonormal basis
which is *explicitly dependent on the crystal wave vector*
:math:`\bm{k}`. Two basis explicitly depending on :math:`\bm{k}` are
commonly considered: Bloch sums and plane waves. Concrete examples of
the usage of Bloch sums and plane waves are going to be presented when
discussing the numerical calculation of the energy spectrum of TBG for,
respectively, the tight-binding and continuum model. For the moment let
just assume to have an orthonormal basis

.. math::
       \{|\phi_{1}(\bm{k})\rangle, \cdots, |\phi_{N}(\bm{k})\rangle\ \} \text{ with } \bm{k}\in{\rm BZ}, \text{ and } \langle \phi_{i}(\bm{k})|\phi_{j}(\bm{k}^{\prime})\rangle = \delta_{ij}\delta_{\bm{k}\bm{k}^{\prime}}
       :label: eq:NUM_basis

and to know the system’s Hamiltonian :math:`\hat{H}` matrix elements on
this basis

.. math:: H_{ij}(\bm{k}) \equiv \langle \phi_{i}(\bm{k})|\hat{H}|\phi_{j}(\bm{k})\rangle.

The matrix whose elements are :math:`H_{ij}(\bm{k})` is Hermitian by
construction, and the spectral theorem assure it has :math:`N` real
eigenvalues. Let us now call these eigenvalues
:math:`\epsilon_{\bm{k}\nu}` and
:math:`(c_{1}(\bm{k},\nu), \ldots, c_{N}(\bm{k},\nu)) \in \mathbb{C}^{N}`
the corresponding eigenvectors, with :math:`\nu = 1, \cdots , N`
indexing the different eigenvalues. The quantities
:math:`\epsilon_{\bm{k}\nu}` and
:math:`(c_{1}(\bm{k},\nu), \ldots, c_{N}(\bm{k},\nu))` are routinely
obtained numerically thanks to `standard diagonalization algorithms based on the “divide and conquer”
approach`_, with a computational cost of the order of :math:`\sim N^{3}`
floating-point operations. The Bloch eigenstate at crystal momentum
:math:`\bm{k}` and (band) index :math:`\nu` is given simply by

.. math:: |\bm{k}\nu\rangle = \sum_{i = 1}^{N} c_{i}(\bm{k}, \nu)|\phi_{i}(\bm{k})\rangle.

By knowing the matrix elements of a generic operator :math:`\hat{O}` on
the basis Eq. `[eq:NUM_basis]`_,

.. math:: O_{ij}(\bm{k},\bm{k}^{\prime}) = \langle \phi_{i}(\bm{k})|\hat{O}|\phi_{j}(\bm{k}^{\prime})\rangle,

the matrix elements of the same operator on the Bloch eigenstates are
readily obtained

.. math:: \langle\bm{k}\nu|\hat{O}|\bm{k}\nu^{\prime}\rangle = \sum_{i = 1}^{N} \sum_{j=1}^{N} c_{i}(\bm{k}, \nu)^{*}O_{ij}(\bm{k},\bm{k}^{\prime})c_{j}(\bm{k}, \nu),

which is just a vector-matrix-vector product. These remarks, albeit
being rather trivial linear algebra facts, allow to actually compute the
matrix elements appearing in the electrical conductivity
Eq. `[eq:local_cond_crystal]`_, and density-density response function
Eqs. `[eqs:density_density_matrix_elements]`_.

.. _standard diagonalization algorithms based on the “divide and conquer” approach: https://numpy.org/doc/stable/reference/generated/numpy.linalg.eigh.html
.. _`[eq:NUM_basis]`: #eq:NUM_basis
.. _`[eq:local_cond_crystal]`: #eq:local_cond_crystal
.. _`[eqs:density_density_matrix_elements]`: #eqs:density_density_matrix_elements

.. [1]
   For the sake of simplicity we explicitly work with two-dimensional systems in this Section. Pybandstructure, however, can handle models of any spatial dimension.