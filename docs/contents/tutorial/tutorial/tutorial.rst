Tutorial
=========
Import packages
"""""""""""""""

.. code:: ipython3

    #import os                        # operating system (creation-deletion files and folders)
    import sys
    import numpy as np               # arrays and mathematical operations
    import matplotlib.pyplot as plt  # plotting
    #sys.path.insert(0, '..')
    from pybandstructure import *

Graphene parameters
"""""""""""""""""""

.. math::  {\cal H}({\bf k}) = \hbar v_{\rm F}[\sigma_x k_x + \sigma_y k_y ]+ \Delta \,\sigma_z

.. code:: ipython3

    #distances in nm, energies in eV
    hv_F = 0.658   # \hbar v_F in eV*nm
    delta = 0.000  # gap in eV

Sampling of reciprocal space
""""""""""""""""""""""""""""

We use a square sampling in the region

.. math:: -k_{cutoff} \leq k_x \leq k_{cutoff}

\ 

.. math:: -k_{cutoff} \leq k_y \leq k_{cutoff}

with :math:`n_{sites} \times n_{sites}` points

.. code:: ipython3

    k_cutoff = 2. #nm^-1 energies are accurate up to +-hv_F * k_cutoff
    n_sites = 201 # integer, better if odd
    print('E_max = ',hv_F * k_cutoff)
    print('Energy_resolution = ', hv_F * k_cutoff /n_sites)


.. parsed-literal::

    E_max =  1.316
    Energy_resolution =  0.00654726368159204


Points are stored as rational coordinates with respect to a basis in the
form

.. math:: k(n_1,n_2) = {\bf G}_1 \frac{n_1}{n_{sites}} + {\bf G}_2 \frac{n_2}{n_{sites}}

.. code:: ipython3

    reciprocal_lattice_basis = 2 * k_cutoff * np.array([[1.,0.],
                                                        [0.,1.]])
    
    k_sample = sample.Space_Sample.centered_cubic_sample(basis_vectors = reciprocal_lattice_basis, 
                                                              denominator = n_sites)
    k_sample.plot()

.. image:: output_7_1.png


Define point group
""""""""""""""""""

Point group (set of rotation and reflections that leave the lattice
unchanged)

Use of symmetry reduces the number of calculations needed

.. code:: ipython3

    point_group = point_groups.Point_Group(space_dimension = 2)  
    point_group2 = point_groups.D6_2D#trivial group (group with only the identity operation)

Define crystal geometry
"""""""""""""""""""""""

Contains all the geometrical information about the lattice (lattice and
symmetry group)

.. code:: ipython3

    lattice_geometry = Crystal_Geometry(reciprocal_lattice_basis = reciprocal_lattice_basis, 
                                        point_group = point_group)

Define Hamiltonian
""""""""""""""""""

.. math::  {\cal H}({\bf k}) = \hbar v_{\rm F}[\sigma_x k_x + \sigma_y k_y ]+ \Delta \,\sigma_z

Operators are defined as linear combinations of matrices and
coefficients

.. math::  O({\bf k}) = \sum_j f_j({\bf k}) M^{(j)}

.. code:: ipython3

    H_Dirac = hv_F * Momentum_Conserving_Operator(matrices = [pauli_matrix('x'), pauli_matrix('y')],
                                            coefficients = [lambda k : k[0], lambda k : k[1]]) 
    
    H_gap = Momentum_Conserving_Operator(matrices = [pauli_matrix('z')],
                                         coefficients = [delta]) 
    H = H_Dirac + H_gap

Define momentum operator
""""""""""""""""""""""""

.. math::  p_i({\bf k}) = \partial_{k_i} {\cal H}({\bf k}) 

.. code:: ipython3

    p_x = Momentum_Conserving_Operator(matrices = [pauli_matrix('x'), pauli_matrix('y')],
                                       coefficients =[hv_F ,0])
    p_y = Momentum_Conserving_Operator(matrices = [pauli_matrix('x'), pauli_matrix('y')],
                                       coefficients = [0,hv_F ])

Initialize band structure
"""""""""""""""""""""""""

.. code:: ipython3

    ################### these parameters can be changed later ##############
    density = 0. # nm^-2
    temperature = 0.026 # k_B T in eV
    eta = 0.020 # eV energy broadening \hbar/tau 
    ########################################################################
    #initialization
    band_structure = Band_Structure.from_hamiltonian(k_sample = k_sample,
                                                     hamiltonian = H, 
                                                     momentum_operator= [p_x,p_y],
                                                     degeneracy = 4,
                                                     zero_filling = 1,
                                                     density = density,
                                                     temperature = temperature)

Calculate band structure
""""""""""""""""""""""""

.. code:: ipython3

    band_structure.compute_bands()
    band_structure.compute_momentum_matrix()

Plot band structure along a contour
"""""""""""""""""""""""""""""""""""

.. code:: ipython3

    contour = [(0,0),(n_sites//2,0),(n_sites//2,n_sites//2),(0,0)]
    band_structure.plot(contour, c='r')
    ax = plt.axes([0.80,0.25,0.10,0.10], frameon = False)
    plt.axis('equal')
    plt.plot([k_sample.get_coords(x)[0] for x in contour],[k_sample.get_coords(x)[1] for x in contour])

.. image:: output_21_1.png


Define crystal
""""""""""""""

.. code:: ipython3

    graphene = Crystal(geometry = lattice_geometry,
                       band_structure = band_structure, 
                       eta = eta)

Plot density of states
""""""""""""""""""""""

compare with analitical formula

.. math::  DOS(E) = \frac{2|E|}{\pi \hbar^2 v_{\rm F}^2}

.. code:: ipython3

    plt.xlabel(r'$E \, [{\rm eV}]$')
    plt.ylabel(r'$DOS \, [{\rm eV}^{-1}{\rm nm}^{-2}]$')
    plt.ylim(0,2)
    energies = np.linspace(-1.,1., num = 101)
    plt.plot(energies, graphene.dos(energies), 'k') 
    plt.plot(energies, 2 *abs(energies)/ (np.pi * hv_F**2),'--', c='g', lw=0.5)

.. image:: output_25_1.png


Plot density as a function of chemical potential
""""""""""""""""""""""""""""""""""""""""""""""""

compare with analitical formula (at :math:`T=0`)

.. math::  n(\mu) = \frac{sign(\mu)\mu^2}{\pi \hbar^2 v_{\rm F}^2}

.. code:: ipython3

    plt.xlabel(r'$\mu \, [{\rm eV}]$')
    plt.ylabel(r'$n\, [{\rm nm}^{-2}]$')
    mu_vals = np.linspace(-0.3,0.3, num = 100)
    density_vals = np.array([graphene.band_structure.compute_density(chemical_potential = mu,temperature=graphene.band_structure.temperature) for mu in mu_vals])
    
    plt.plot(mu_vals, density_vals, c= 'k')
    plt.plot(mu_vals, np.sign(mu_vals) * mu_vals**2/(np.pi * hv_F**2),'--', c='g', lw=0.5)

.. image:: output_27_1.png


Theromoelectric quantities (In the relaxation time approximation)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Drude weight and dc conductivity

.. math:: \sigma_0 = \frac{{\cal D}\tau}{\pi}

 Thermal (electronic) conductivity

.. math:: \kappa_0 = \sigma_0 T {\cal L} 

 Wiedemann-Franz law

.. math:: {\cal L} = {\cal L}_0 =\frac{\pi^2k_{\rm B}^2}{3e^2}

.. code:: ipython3

    W0 = graphene.generalized_drude_weight(chemical_potential = mu_vals,exponent=0)[:,0,0]
    W1 = graphene.generalized_drude_weight(chemical_potential = mu_vals,exponent=1)[:,0,0]
    W2 = graphene.generalized_drude_weight(chemical_potential = mu_vals,exponent=2)[:,0,0]
    
    lorentz_number = 3./(np.pi**2* graphene.band_structure.temperature**2)*W2/W0
    seebeck = -W1/(W0 * graphene.band_structure.temperature)

Drude weight
""""""""""""

Compare with the :math:`T=0` formula

.. math::  {\cal D}= |\mu| e^2/\hbar^2

.. code:: ipython3

    plt.xlabel(r'$\mu \, [{\rm eV}]$')
    plt.ylabel(r'${\cal D}\hbar^2/e^2$')
    plt.ylim(0,np.amax(W0))
    
    plt.plot(mu_vals,W0,'k-')
    plt.plot(mu_vals,abs(mu_vals),'g""',lw=0.5)




.. parsed-literal::

    [<matplotlib.lines.Line2D at 0x1a026ec5c88>]




.. image:: output_31_1.png


Seebeck coefficient
"""""""""""""""""""

Compare with Mott’s formula (black-dashed) and with the Fermi-liquid
result (green)

.. math:: S = -\frac{\pi^2k_{\rm B}^2 T }{3e\mu}

.. code:: ipython3

    plt.xlabel(r'$\mu \, [{\rm eV}]$')
    plt.ylabel(r'$Se/k_{\rm B}$')
    plt.ylim(-np.amax(seebeck)*1.1,np.amax(seebeck)*1.1)
    
    plt.plot(mu_vals,seebeck,'k-')
    plt.plot(mu_vals,-np.pi**2/3 * graphene.band_structure.temperature*np.gradient(W0,mu_vals)/W0,'k""',lw=0.5)
    plt.plot(mu_vals,-np.pi**2/3 * graphene.band_structure.temperature/mu_vals,'g""',lw=0.5)
    plt.plot(mu_vals,np.zeros_like(mu_vals),'k-',lw=0.5)




.. parsed-literal::

    [<matplotlib.lines.Line2D at 0x1a026f34688>]




.. image:: output_33_1.png


Lorentz ratio
"""""""""""""

Compare with the Wiedemann-Franz law

.. math:: \frac{{\cal L}}{{\cal L}_0} = 1 

.. code:: ipython3

    plt.ylabel(r'${\cal L}/{\cal L}_0$')
    plt.xlabel(r'$\mu \, [{\rm eV}]$')
    plt.ylim(0,np.amax(lorentz_number)*1.1)
    
    plt.plot(mu_vals, lorentz_number, 'k-')
    plt.plot(mu_vals, np.ones_like(mu_vals),'g""',lw=0.5)




.. parsed-literal::

    [<matplotlib.lines.Line2D at 0x1a026f9cdc8>]




.. image:: output_35_1.png


Calculate conductivity
""""""""""""""""""""""

.. code:: ipython3

    omega_vals = np.linspace(0,1, num = 100)
    sigma_vals = graphene.local_conductivity(omega_vals) # result in units of G_0, the quantum of conductance


.. parsed-literal::

    0it [00:00, ?it/s]C:\Users\Windows\anaconda3\lib\site-packages\pybandstructure\crystal\analysis\conductivity_functions.py:9: RuntimeWarning: invalid value encountered in true_divide
      prefactor = - occupation_diff / energy_diff
    100it [00:09, 10.33it/s]


Plot conductivity
"""""""""""""""""

Interband conductivity conveges to

.. math:: \sigma_{\rm uni } = \frac{\pi G_0}{4 } = \frac{\pi e^2}{2 h} = \frac{e^2}{4\hbar}

.. code:: ipython3

    plt.xlabel(r'$\hbar \omega \, [{\rm eV}]$')
    plt.ylabel(r'$\sigma(\omega)/G_0$')
    plt.ylim(-3,3)
    plt.plot(omega_vals,np.real(sigma_vals[:,1,0]),c='k') #note that sigma is a matrix, here I am selecting the xx component
    plt.plot(omega_vals,np.imag(sigma_vals[:,1,0]),'--',c='k')
    
    plt.plot(omega_vals,np.ones_like(omega_vals)*np.pi/4,'--', c='g', lw=0.5)
    plt.plot(omega_vals,np.zeros_like(omega_vals),'k-',lw=0.5)

.. image:: output_39_1.png


Dynamically change density, chemical potential or temperature
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. code:: ipython3

    graphene.band_structure.density = 0.05
    print(graphene.band_structure.chemical_potential)
    graphene.band_structure.plot(contour, c='r')


.. parsed-literal::

    0.25648784715817685

.. image:: output_41_3.png


Recalculate optical conductivity
""""""""""""""""""""""""""""""""

Conductivity is redistributed due to Pauli blocking effect

.. code:: ipython3

    sigma_vals = graphene.local_conductivity(omega_vals) # result in units of G_0, the quantum of conductance
    
    plt.xlabel(r'$\hbar \omega \, [{\rm eV}]$')
    plt.ylabel(r'$\sigma(\omega)/G_0$')
    plt.ylim(-3,3)
    plt.plot(omega_vals,np.real(sigma_vals[:,0,0]),c='k') #note that sigma is a matrix, here I am selecting the xx component
    plt.plot(omega_vals,np.imag(sigma_vals[:,0,0]),'--',c='k')
    plt.plot(omega_vals,np.heaviside(omega_vals-2*abs(graphene.band_structure.chemical_potential),0.5)*np.pi/4, '--',c='g', lw=0.5)
    
    plt.plot(omega_vals,np.zeros_like(omega_vals),'k-',lw=0.5)

.. image:: output_43_2.png

