
Documentation of Pybandstructure
===========================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   contents/introduction/main
   contents/introduction/technical

   contents/tutorial/tutorial/tutorial

   contents/api/reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
